;; -*- lexical-binding: t; -*-
(require 'my:core)
(require 'my:code)

(use-package go-mode
  :defer t
  :gfhook ('go-mode-hook
           '(ggtags-mode lsp-deferred))
  :preface
  (general-create-definer my:golang::general-def
    :wrapping my:core::mmode-general-def
    :prefix-command #'my:golang::comand
    :prefix-map 'my:golang:map
    :keymaps 'go-mode-map
    :major-modes t)
  :init
  (setq go-test-verbose t
        godoc-at-point-function #'godoc-gogetdoc)
  :ensure-system-package (gogetdoc . "go get github.com/zmb3/gogetdoc")
  :config
  (progn
    (my:golang::general-def
      "=" #'gofmt
      "e" '(:ignore t :wk "playground")
      "eb" #'go-play-buffer
      "ed" #'go-download-play
      "er" #'go-play-region
      "ga" #'ff-find-other-file
      "gc" #'go-coverage
      
      "h" #'godoc-at-point

      "i" '(:ignore t :wk "import")
      "ig" #'go-goto-imports
      "ir" #'go-remove-unused-imports)
    (my:code::ggtags-def
      :keymaps 'go-mode-map :package 'go-mode)))
(use-package go-complete
  :after go-mode
  :config
  (compdef
   :modes 'go-mode
   :capf '(lsp-completion-at-point
           go-complete-at-point
           dabbrev-completion
           t)))
(use-package gotest
  :after go-mode
  :general
  (my:golang::general-def
    "t" '(:ignore t :wk "test")
    "tt" #'go-test-current-test
    "tf" #'go-test-current-file
    "tp" #'go-test-current-project
    "tc" #'go-test-current-coverage
    "tb" '(:ignore t :wk "benchmark")
    "tbb" #'go-test-current-benchmark
    "tbf" #'go-test-current-file-benchmarks
    "tbp" #'go-test-current-project-benchmarks))

(use-package helm-go-package
  :after go-mode
  :general
  (my:golang::general-def
    "i" '(:ignore t :wk "import")
    "ia" #'go-import-add))
(use-package go-rename
  :after go-mode
  :general
  (my:golang::general-def
    "r" '(:ignore t :wk "refactor")
    "rN" 'go-rename))
(use-package go-tag
  :after go-mode
  :general
  (my:golang::general-def
    "r" '(:ignore t :wk "refactor")
    "rf" 'go-tag-add
    "rF" 'go-tag-remove))
(use-package godoctor
  :after go-mode
  :general
  (my:golang::general-def
    "r" '(:ignore t :wk "refactor")
    "rd" 'godoctor-godoc
    "re" 'godoctor-extract
    "rn" 'godoctor-rename
    "rt" 'godoctor-toggle))
(use-package go-fill-struct
  :after go-mode
  :general
  (my:golang::general-def
    "rs" 'go-fill-struct))
(use-package go-impl
  :after go-mode
  :general
  (my:golang::general-def
    "r" '(:ignore t :wk "refactor")
    "ri" #'go-impl))

(use-package go-guru
  :after go-mode
  :general
  (my:golang::general-def
    "g" '(:ignore t :wk "goto")
    "g<" 'go-guru-callers
    "g>" 'go-guru-callees
    "gc" 'go-guru-peers
    "gd" 'go-guru-describe
    "ge" 'go-guru-whicherrs
    "gf" 'go-guru-freevars
    "gi" 'go-guru-implements
    "gj" 'go-guru-definition
    "gg" 'go-guru-definition
    "go" 'go-guru-set-scope
    "gp" 'go-guru-pointsto
    "gr" 'go-guru-referrers
    "gs" 'go-guru-callstack))
(use-package go-eldoc
  :gfhook ('go-mode-hook #'go-eldoc-setup))
(use-package flycheck-golangci-lint
  :gfhook ('go-mode-hook #'flycheck-golangci-lint-setup))

(provide 'my:golang)
