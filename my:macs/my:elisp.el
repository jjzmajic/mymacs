;; -*- lexical-binding: t; -*-
(require 'my:core)
(require 'my:code)

(use-package eval-sexp-fu
  :defer t 
  :commands eval-sexp-fu-flash-mode
  :init (general-create-definer my:elisp::general-def
          :wrapping my:core::mmode-general-def
          :prefix-command #'my:elisp::command
          :prefix-map 'my:elisp:map
          :keymaps
          '(emacs-lisp-mode-map
            lisp-interaction-mode-map)
          :major-modes t)
  :gfhook
  ('emacs-lisp-mode-hook
   #'eval-sexp-fu-flash-mode))
(use-package ielm
  :after semantic
  :gfhook
  ('emacs-lisp-mode-hook
   '(hs-hide-all flycheck-mode))
  :init
  (progn
    (add-to-list
     'semantic-new-buffer-setup-functions
     '(emacs-lisp-mode . semantic-default-elisp-setup))
    (my:elisp::general-def
      "," #'eval-defun
      "'" #'ielm
      "l" #'lisp-state-toggle-lisp-state

      "e$" #'lisp-state-eval-sexp-end-of-line
      "c" '(:ignore t :wk "compile")
      "cc" #'emacs-lisp-byte-compile

      "s" '(:ignore t :wk "shell")
      "si" #'ielm

      "e" '(:ignore t :wk "eval")
      "eb" #'eval-buffer
      "ed" #'eval-defun
      "ee" #'eval-last-sexp
      "er" #'eval-region
      "ef" #'eval-defun
      "el" #'lisp-state-eval-sexp-end-of-line

      "t" '(:ignore t :wk "test")
      "tq" #'ert)
    (my:code::ggtags-def
      :keymaps
      '(emacs-lisp-mode-map
        lisp-interaction-mode-map)))
  :config
  (compdef
   :modes '(emacs-lisp-mode lisp-interaction-mode ielm-mode)
   :capf '(helm-lisp-completion-or-file-name-at-point
           ggtags-completion-at-point
           t)))
(use-package eldoc
  :gfhook ('emacs-lisp-mode-hook
           #'eldoc-mode))

(use-package overseer
  :defer t
  :general
  (my:elisp::general-def
    "t" '(:ignore t :wk "test")
    "ta" #'overseer-test
    "tt" #'overseer-test-run-test
    "tb" #'overseer-test-this-buffer
    "tf" #'overseer-test-file
    "tg" #'overseer-test-tags
    "tp" #'overseer-test-prompt
    "tA" #'overseer-test-debug
    "tq" #'overseer-test-quiet
    "tv" #'overseer-test-verbose
    "th" #'overseer-help))
(use-package edebug
  :defer t
  :config
  (progn
    (defun my:elisp::instrument-defun ()
      (interactive)
      (eval-defun t))
    (hercules-def
     :show-funs '(edebug-mode)
     :keymap 'edebug-mode-map
     :transient t)
    (hercules-def
     :show-funs '(debugger-mode)
     :keymap 'debugger-mode-map
     :transient t))
  :general
  (my:elisp::general-def
    "d" '(:ignore t :wk "debug")
    "dd" #'edebug-defun
    "di" #'my:elisp::instrument-defun
    "de" #'debug-on-entry
    "df" #'elp-instrument-function
    "dl" #'elp-instrument-list
    "dp" #'elp-instrument-package))
(use-package macrostep
  :defer t
  :config
  (hercules-def
   :toggle-funs #'macrostep-mode
   :keymap 'macrostep-keymap)
  :general
  (my:elisp::general-def
    "m" '(:ignore t :wk "macrostep")
    "m." #'macrostep-mode
    "me" #'macrostep-expand
    "mc" #'macrostep-collapse
    "mn" #'macrostep-next-macro
    "mp" #'macrostep-prev-macro))

(use-package helpful
  :defer t
  :general
  (my:core::general-def
    "k" '(:ignore t :wk "key")
    "kk" #'helpful-key

    "h" '(:ignore t :wk "help")
    "hv" #'helpful-variable
    "hf" #'helpful-callable
    "hF" #'helpful-function
    "hm" #'helpful-macro
    "hM" #'describe-mode
    "hp" #'describe-package
    "ht" #'describe-theme
    "hc" #'helpful-command
    "hC" #'describe-char))
(use-package elisp-slime-nav
  :gfhook ('emacs-lisp-mode-hook
           #'elisp-slime-nav-mode)
  :general
  (my:elisp::general-def
    "g" #'elisp-slime-nav-find-elisp-thing-at-point
    "h"  #'helpful-at-point
    "H"  #'elisp-slime-nav-describe-elisp-thing-at-point))
(use-package srefactor-lisp
  :straight nil
  :defer t
  :after semantic
  :init (require 'srefactor)
  :commands (srefactor-lisp-format-buffer
             srefactor-lisp-format-defun
             srefactor-lisp-format-sexp
             srefactor-lisp-one-line)
  :config (semantic-default-elisp-setup)
  :general
  (my:elisp::general-def
    "=" '(:ignore t :wk "format")
    "=b" #'srefactor-lisp-format-buffer
    "=d" #'srefactor-lisp-format-defun
    "=o" #'srefactor-lisp-one-line
    "=s" #'srefactor-lisp-format-sexp))

(use-package cl-lib-highlight
  :defer t
  :gfhook ('emacs-lisp-mode-hook
           #'cl-lib-highlight-initialize))
(use-package nameless
  :defer t
  :general (my:elisp::general-def
            "T" '(:ignore t :wk "toggle")
            "Tn" #'nameless-mode))

(use-package flycheck-package
  :defer t
  :init (setq flycheck-emacs-lisp-load-path 'inherit)
  :gfhook ('emacs-lisp-mode #'flycheck-package-setup))

(provide 'my:elisp)
