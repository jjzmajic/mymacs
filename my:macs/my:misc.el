;; -*- lexical-binding: t; -*-
;; programming
(use-package indent-guide
  :gfhook ('prog-mode-hook #'indent-guide-mode))
(use-package display-line-numbers
  :init (setq display-line-numbers-type 'relative)
  :config (global-display-line-numbers-mode +1))
(use-package rainbow-mode
  :gfhook ('prog-mode-hook #'rainbow-mode))
(use-package rainbow-delimiters
  :defer t
  :init (electric-pair-mode)
  :gfhook
  ('prog-mode-hook
   #'rainbow-delimiters-mode))
(use-package fill-column-indicator
  :defer t
  :gfhook ('git-commit-mode-hook #'fci-mode))
(use-package string-inflection
  :defer t
  :general
  (my:core::general-def
    "x" '(:ignore t :wk "text")
    "xi" '(:ignore t :wk "inflection")
    "xiC" #'string-inflection-camelcase
    "xic" #'string-inflection-lower-camelcase
    "xi-" #'string-inflection-kebab-case
    "xi_" #'string-inflection-underscore
    "xiU" #'string-inflection-upcase))
(use-package parinfer
  :defer t
  :init
  (setq parinfer-extensions
        '(defaults       ; should be included.
           pretty-parens  ; different paren styles for different modes.
           evil           ; If you use Evil.
         ;; lispy          ; If you use Lispy. Do not enable lispy-mode directly.
           paredit        ; Introduce some paredit commands.
           smart-tab      ; C-b & C-f jump positions and smart shift with tab & S-tab.
           smart-yank)   ; Yank behavior depend on mode.
        )
  :general (my:core::general-def
             "t" '(:ignore t :wk "toggle")
             "tp" #'parinfer-toggle-mode))
(use-package hl-todo
  :defer t
  :gfhook
  ('(prog-mode-hook text-mode-hook)
   #'hl-todo-mode))
(use-package highlight-numbers
  :defer t
  :gfhook
  ('prog-mode-hook
   #'highlight-numbers-mode))

;; emacs
(use-package eshell
  :defer t
  :init
  (defun my:misc::setup-eshell ()
    "Set up eshell given that the keymap is buffer local."
    (eshell-cmpl-initialize)
    (general-def
      :keymaps 'eshell-mode-map
      :package 'eshell
      [remap eshell-pcomplete] #'helm-esh-pcomplete
      "M-r" #'helm-eshell-history))
  :gfhook ('eshell-mode-hook #'my:misc::setup-eshell)
  :general
  (my:core::general-def
    "'" #'eshell
    "ae" #'eshell
    "be" #'helm-eshell-prompts-all))
(use-package restart-emacs
  :defer t
  :init (setq make-backup-files nil)
  :general
  (my:core::general-def
    "q" '(:ignore t :wk "quit")
    "qr" #'restart-emacs))
(use-package pcre2el
  :defer t)
(use-package terminal-here
  :straight t
  :defer t
  :init
  (progn
    (setq my:misc:termial "termite")
    (when (executable-find my:misc:termial)
      (setq terminal-here-terminal-command
            `(,my:misc:termial))))
  :general
  (my:core::general-def
    "\"" #'terminal-here-launch
    "p" '(:ignore t :wk "project")
    "p\"" #'terminal-here-project-launch))
(use-package anzu
  :config (global-anzu-mode +1))
(use-package smooth-scrolling
  :config (smooth-scrolling-mode +1))
(use-package emacs-anywhere
  :no-require t
  :straight nil
  :preface
  (progn
    (defun my:misc::ea-github-conversation-p (window-title)
      (or (string-match-p "Pull Request" window-title)
          (string-match-p "Issue" window-title)))
    (defun my:misc::ea-popup-handler (app-name window-title x y w h)
      (cond
       ((my:github-conversation-p window-title) (gfm-mode))
       (t (markdown-mode))))
    (when (not (file-directory-p "~/.emacs_anywhere"))
      (shell-command (concat "curl -fsSL " "https://raw.github.com/"
                             "zachcurry/emacs-anywhere/master/install"
                             "| bash"))))
  :gfhook ('ea-popup-hook #'my:misc::ea-popup-handler))
(use-package exec-path-from-shell
  :init (setq exec-path-from-shell-variables
              '("PATH" "MANPATH"
                "GOPATH" "NODE_PATH"))
  :config (exec-path-from-shell-initialize))
(use-package direnv
  :config (direnv-mode))
(use-package ialign
  :disabled t
  :general (my:core::general-def
             "xaa" #'ialign))

;; misc
(use-package helm-system-packages
  :defer t
  :general (my:core::general-def
             "hP" #'helm-system-packages))

(provide 'my:misc)
