;; -*- lexical-binding: t; -*-
(use-package json-mode
  :mode "\\.js\\(?:on\\|[hl]int\\(?:rc\\)?\\)\\'"
  :defer t
  :init
  (general-create-definer my:json::general-def
    :wrapping my:core::mmode-general-def
    :keymaps 'json-mode-map
    :major-modes t))

(use-package json-reformat
  :after json-mode
  :general (my:json::general-def
            "=" #'json-reformat-region))
(use-package json-navigator
  :after json-mode
  :general
  (my:json::general-def
    "h" '(:ignore t :wk "help")
    "hh" #'json-navigator-navigate-after-point))
(use-package json-snatcher
  :after json-mode
  :general
  (my:json::general-def
    "hp" #'json-print-path))

(provide 'my:json)
