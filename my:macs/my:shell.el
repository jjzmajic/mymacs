;; -*- lexical-binding: t; -*-
(require 'my:core)
(require 'my:code)

(use-package sh-script
  :defer t
  :mode (("\\.zsh\\'" . sh-mode)
         ("zlogin\\'" . sh-mode)
         ("zlogout\\'" . sh-mode)
         ("zpreztorc\\'" . sh-mode)
         ("zprofile\\'" . sh-mode)
         ("zshenv\\'" . sh-mode)
         ("zshrc\\'" . sh-mode)
         ("zshutil\\'" . sh-mode))
  :init
  (progn
    (general-create-definer my:shell::general-def
      :wrapping my:core::mmode-general-def
      :keymaps 'sh-mode-map
      :prefix-map 'my:shell:map
      :prefix-command #'my:shell::command)
    (defun my:shell::setup-zsh ()
      (when (and buffer-file-name
                 (string-match-p "\\.zsh\\'" buffer-file-name))
        (sh-set-shell "zsh"))))
  :gfhook
  ('sh-mode-hook
   '(my:shell::setup-zsh
     lsp-deferred
     ggtags-mode))
  :config
  (progn
    (compdef
     :modes 'sh-mode
     :capf '(lsp-completion-at-point
             sh-completion-at-point-function
             comint-completion-at-point
             ggtags-completion-at-point
             t))
    (my:shell::general-def
      "\\" #'sh-backslash-region
      "i" '(:ignore t :wk "insert")
      "ic" #'sh-case
      "ii" #'sh-if
      "if" #'sh-function
      "io" #'sh-for
      "ie" #'sh-indexed-loop
      "iw" #'sh-while
      "ir" #'sh-repeat
      "is" #'sh-select
      "iu" #'sh-until
      "ig" #'sh-while-getopts)
    (my:code::ggtags-def
      :keymaps 'sh-mode-map)))
(use-package fish-mode
  :defer t)

(provide 'my:shell)
