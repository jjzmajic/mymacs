;; -*- lexical-binding: t; -*-
(use-package auctex
  :defer t
  :no-require t
  :preface (general-create-definer my:latex::general-def
             :wrapping my:core::mmode-general-def
             :keymaps 'TeX-mode-map
             :prefix-map 'my:latex:map
             :prefix-command #'my:latex::command))
(use-package tex
  :straight nil
  :defer t
  :after flycheck
  :init
  (progn
    (setq TeX-engine 'luatex)
    (compdef
     :modes '(tex-mode latex-mode)
     :capf '(TeX-complete-symbol
             ;; latex-complete-data
             ;; latex-complete-refkeys
             ;; latex-complete-envnames
             ;; latex-complete-bibtex-keys
             dabbrev-completion
             t))
    (defun my:latex::build ()
      (interactive)
      (progn
        (let ((TeX-save-query nil))
          (TeX-save-document (TeX-master-file)))
        (TeX-command "LatexMk"
                     'TeX-master-file -1))))
  :gfhook ('LaTeX-mode-hook
           '(TeX-fold-mode auto-fill-mode))
  :general
  (my:latex::general-def
    "%" #'TeX-comment-or-uncomment-paragraph
    "*" #'LaTeX-mark-section
    "-" #'TeX-recenter-output-buffer
    "." #'LaTeX-mark-environment
    ";" #'TeX-comment-or-uncomment-region
    "\\" #'TeX-insert-macro
    "a" #'TeX-command-run-all
    "b" #'my:latex::build
    "c" #'LaTeX-close-environment
    "e" #'LaTeX-environment
    "k" #'TeX-kill-job
    "l" #'TeX-recenter-output-buffer
    "L" #'TeX-next-error
    "m" #'TeX-insert-macro
    "s" #'LaTeX-section
    "v" #'TeX-view
    "h" #'TeX-doc
    "G" '(my:code::ggtags-command :wk "ggtags")

    ;; figure out if it's really needed or if
    ;; evil is smart enough
    "z" '(:ignore t :wk "zhow")
    "z=" #'TeX-fold-math
    "zB" #'TeX-fold-clearout-buffer
    "zI" #'TeX-fold-clearout-item
    "zP" #'TeX-fold-clearout-paragraph
    "zR" #'TeX-fold-clearout-region
    "zb" #'TeX-fold-buffer
    "ze" #'TeX-fold-env
    "zm" #'TeX-fold-macro
    "zp" #'TeX-fold-paragraph
    "zr" #'TeX-fold-region
    "zz" #'TeX-fold-dwim

    "i" '(:ignore t :wk "insert")
    "ii" #'LaTeX-insert-item
    "ie" #'LaTeX-insert-environment
    "ii" #'TeX-insert-macro

    "f" '(:ignore t :wk "fill")
    "fe" #'LaTeX-fill-environment
    "fp" #'LaTeX-fill-paragraph
    "fr" #'LaTeX-fill-region
    "fs" #'LaTeX-fill-section

    "p" '(:ignore t :wk "preview")
    "pb" #'preview-buffer
    "pc" #'preview-clearout
    "pd" #'preview-document
    "pe" #'preview-environment
    "pf" #'preview-cache-preamble
    "pp" #'preview-at-point
    "pr" #'preview-region
    "ps" #'preview-section)
  :config
  (progn
    (add-to-list 'TeX-view-program-selection
                 '(output-pdf "xdg-open"))))

(use-package calctex
  :after tex
  :straight
  (calctex
   :type git
   :host github
   :repo "johnbcoughlin/calctex"))
(use-package auctex-latexmk
  :defer t
  :gfhook ('TeX-mode-hook #'auctex-latexmk-setup))
(use-package reftex
  :after tex
  :general
  (my:latex::general-def
    "r TAB" 'reftex-index
    "rI"    'reftex-display-index
    "rP"    'reftex-index-visit-phrases-buffer
    "rT"    'reftex-toc-recenter
    "rc"    'reftex-citation
    "rg"    'reftex-grep-document
    "ri"    'reftex-index-selection-or-word
    "rl"    'reftex-label
    "rp"    'reftex-index-phrase-selection-or-word
    "rr"    'reftex-reference
    "rs"    'reftex-search-document
    "rt"    'reftex-toc
    "rv"    'reftex-view-crossref))
(use-package magic-latex-buffer
  :after tex
  :init (setq magic-latex-enable-pretty-symbols nil)
  :gfhook ('TeX-update-style-hook #'magic-latex-buffer))

(provide 'my:latex)
