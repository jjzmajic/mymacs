;; -*- lexical-binding: t; -*-

;; git
(use-package magit
  :defer t
  :commands (magit-status)
  :config (general-def
            :keymaps 'magit-mode-map
            "TAB" #'magit-section-toggle
            "<tab>" #'magit-section-toggle)
  :general (my:core::general-def
            "g" '(:ignore t :wk "git")
            "gc"  'magit-clone

            "gf" '(:ignore t :wk "file")
            "gff" #'magit-find-file
            "gfl" #'magit-log-buffer-file
            "gfd" #'magit-diff
            "gi" #'magit-init
            "gL" #'magit-list-repositories
            "gm" #'magit-dispatch
            "gs" #'magit-status
            "gS" #'magit-stage-file
            "gU" #'magit-unstage-file
            "gs" #'magit-status))
(use-package evil-magit
  :after magit)
(use-package magit-gitflow
  :defer t
  :hook (magit-mode . turn-on-magit-gitflow)
  :general (:keymaps 'magit-mode-map
            "%" 'magit-gitflow-popup))
(use-package magit-todos
  :hook (magit-mode . magit-todos-mode))
(use-package helm-git-grep
  :general (my:core::general-def
  "g/" #'helm-git-grep
  "g*" #'helm-git-grep-at-point))
(use-package helm-gitignore
  :defer t
  :general (my:core::general-def
            "gI" #'helm-gitignore))
(use-package git-commit
  :defer t
  :general (my:core::mmode-general-def
            :keymaps 'with-editor-mode-map
            "," #'with-editor-finish
            "c" #'with-editor-finish
            "k" #'with-editor-cancel))
(use-package git-link
  :defer t
  :init (setq git-link-open-in-browser t)
  :general (my:core::general-def
            "gl" '(:ignore t :wk "link")
            "gll" #'git-link
            "glc" #'git-link-commit))
(use-package git-messenger
  :defer t
  :general (my:core::general-def
            "gM" #'git-messenger:popup-message)
  :general (:keymaps 'git-messenger-map
            "ESC" #'git-messenger:popup-close))
(use-package git-timemachine
  :defer t
  :config (progn
            (hercules-def
             :keymap 'git-timemachine-mode-map
             :show-funs #'git-timemachine
             :hide-funs #'git-timemachine-quit)
            (my:core::general-def "tt" #'git-timemachine-toggle))
  :general (my:core::general-def
  "gt" '(:ignore t :wk "timemachine")
  "gt." #'git-timemachine
  "gtc" #'git-timemachine-show-current-revision
  "gtg" #'git-timemachine-show-nth-revision
  "gtp" #'git-timemachine-show-previous-revision
  "gtn" #'git-timemachine-show-next-revision
  "gtN" #'git-timemachine-show-previous-revision
  "gtY" #'git-timemachine-kill-revision))
(use-package gitattributes-mode
  :defer t)
(use-package gitconfig-mode
  :defer t)
(use-package gitignore-mode
  :after compdef
  :defer t
  :config
  (compdef
   :modes #'gitignore-mode
   :capf '(comint-dynamic-complete-filename t))
  ;; not worth another definer
  :general
  (my:core::mmode-general-def
    :keymaps 'gitignore-mode-map
    "i" 'gitignore-templates-insert))
(use-package gitignore-templates
  :defer t
  :config (my:core::general-def
          "gf" '(:ignore t :wk "file")
          "gfi" #'gitignore-templates-new-file))
(use-package diff-hl
  :defer t
  :commands diff-hl-magit-post-refresh
  :init (setq diff-hl-side 'left)
  :hook (prog-mode . diff-hl-mode)
  :gfhook ('magit-post-refresh-hook #'diff-hl-magit-post-refresh)
  :config (progn
            (global-diff-hl-mode)
            (diff-hl-margin-mode)
            (diff-hl-dired-mode)
            (diff-hl-flydiff-mode)))
(use-package smeargle
  :defer t
  :general (my:core::general-def
  "gH" '(nil :wk "higlight")
  "gHc" 'smeargle-clear
  "gHh" 'smeargle-commits
  "gHt" 'smeargle))

;; project
(use-package dired-sidebar
  :defer t
  :after winum
  :init (setq dired-sidebar-theme 'ascii
              dired-sidebar-width 40
              dired-sidebar-use-term-integration t)
  :config (progn
            (dired-async-mode +1)
            (defun my:code::dired-sidebar-winum-0 ()
              (when (string-suffix-p
                     (buffer-name)
                     (dired-sidebar-buffer-name (buffer-name)))
                0))
            (add-to-list 'winum-assign-functions
                         #'my:code::dired-sidebar-winum-0))
  :general (my:core::general-def
             "j" '(:ignore t :wk "jump")
             "jd" #'dired-jump
             "jD" #'dired-jump-other-window

             "f" '(:ignore t :wk "files")
             "ft" #'dired-sidebar-toggle-sidebar
             "0" #'dired-sidebar-jump-to-sidebar))
(use-package projectile
  :defer t
  :commands (projectile-ack
              projectile-ag
              projectile-compile-project
              projectile-dired
              projectile-find-dir
              projectile-find-file
              projectile-find-tag
              projectile-test-project
              projectile-grep
              projectile-invalidate-cache
              projectile-kill-buffers
              projectile-multi-occur
              projectile-project-p
              projectile-project-root
              projectile-recentf
              projectile-regenerate-tags
              projectile-replace
              projectile-replace-regexp
              projectile-run-async-shell-command-in-root
              projectile-run-shell-command-in-root
              projectile-switch-project
              projectile-switch-to-buffer
              projectile-vc)
  :general (my:core::general-def
             "p" '(:ignore t :wk "project")
             "p!" #'projectile-run-shell-command-in-root
             "p&" #'projectile-run-async-shell-command-in-root
             "p%" #'projectile-replace-regexp
             "pa" #'projectile-toggle-between-implementation-and-test
             "pc" #'projectile-compile-project
             "pD" #'projectile-dired
             "pe" #'projectile-edit-dir-locals
             "pg" #'projectile-find-tag
             "pG" #'projectile-regenerate-tags
             "pI" #'projectile-invalidate-cache
             "pk" #'projectile-kill-buffers
             "pR" #'projectile-replace
             "pT" #'projectile-test-project
             "pv" #'projectile-vc)
  :config (projectile-mode))
(use-package helm-projectile
  :commands
  (helm-projectile-switch-to-buffer
   helm-projectile-find-dir
   helm-projectile-dired-find-dir
   helm-projectile-recentf
   helm-projectile-find-file
   helm-projectile-grep
   helm-projectile
   helm-projectile-switch-project)
  :init
  (defmacro my:code::search-project (project &optional files)
    (let* ((emacs-name (symbol-name (eval project)))
           (f-or-m (if files "files" "modules"))
           (emacs-dir (if (string= emacs-name "my:macs")
                          user-emacs-directory
                        (straight--repos-dir emacs-name)))
           (func-symbol (intern (concat "my:code::search-" emacs-name "-" f-or-m)))
           (func-doc (format "Seach %s %s with `helm-projectile'."
                             (upcase emacs-name) (upcase f-or-m)))
           (search-func (if files
                            #'helm-projectile-find-file
                          #'helm-do-ag-project-root)))
      `(progn
         (defun ,func-symbol ()
           ,func-doc
           (interactive)
           (let ((default-directory ,emacs-dir))
             (,search-func)))
         ',func-symbol)))
  :config (helm-projectile-on)
  :general
  (my:core::general-def
    "fe" '(:ignore t :wk "emacs")
    "fem" (my:code::search-project 'my:macs t)
    "fed" (my:code::search-project 'doom-emacs t)
    "fes" (my:code::search-project 'spacemacs t)

    "se" '(:ignore t :wk "emacs")
    "sed" (my:code::search-project 'doom-emacs)
    "ses" (my:code::search-project 'spacemacs)
    "sem" (my:code::search-project 'my:macs)

    "pp" #'helm-projectile-switch-project
    "pf" #'helm-projectile-find-file
    "pF" #'helm-projectile-find-file-dwim
    "pd" #'helm-projectile-find-dir
    "pr" #'helm-projectile-recentf
    "pb" #'helm-projectile-switch-to-buffer
    "ps" #'helm-projectile-ag))

;; snippet
(use-package yasnippet
  :defer t
  :commands (yas-expand-snippet)
  :config (yas-global-mode +1))
(use-package yasnippet-snippets
  :after yasnippet
  :config (yasnippet-snippets-initialize))
(use-package doom-snippets
  :after yasnippet
  :straight (emacs-snippets
             :type git
             :host github
             :repo "hlissner/emacs-snippets"
             :files ("*"))
  :config (doom-snippets-initialize))
(use-package auto-yasnippet
  :defer t
  :init (progn
          (setq aya-persist-snippets-dir
                (concat my:init:macs "my:snippets/")))
  :general (my:core::general-def
             "i" '(:ignore t :wk "insert")
             "iS" '(:ignore t :wk "snippet")
             "iSc" #'aya-create
             "iSe" #'aya-expand
             "iSp" #'aya-persist-snippet))
(use-package helm-c-yasnippet
  :defer t
  :general (my:core::general-def
             "is" #'helm-yas-complete))

;; complete
(use-package compdef
  :straight
  (compdef :type git
           :host gitlab
           :repo "jjzmajic/compdef"
           :depth full))
(use-package lsp-mode
  :defer t
  :commands (lsp-completion-at-point)
  :init
  (setq lsp-prefer-flymake nil
        lsp-enable-snippet nil)
  :general
  (my:core::general-def
    "tl" #'lsp-mode))

;; nav
(use-package ggtags
  :general (my:core::general-def
             "tg" #'ggtags-mode))
(use-package helm-gtags
  :after ggtags
  :init (general-def
          :keymaps 'ggtags-mode-map
          "M-." #'helm-gtags-dwim
          "C-x 4 ." #'helm-gtags-find-tag-other-window
          "M-," #'helm-gtags-pop-stack
          "M-*" #'helm-gtags-pop-stack)
  :if (general-create-definer my:code::ggtags-def
        :wrapping my:core::mmode-general-def
        ;; don't use for major modes
        "f" '(:ignore t :wk "find")
        "fC" 'helm-gtags-create-tags
        "fd" 'helm-gtags-find-tag
        "fD" 'helm-gtags-find-tag-other-window
        "ff" 'helm-gtags-select-path
        "fG" 'helm-gtags-dwim-other-window
        "fi" 'helm-gtags-tags-in-this-function
        "fl" 'helm-gtags-parse-file
        "fn" 'helm-gtags-next-history
        "fp" 'helm-gtags-previous-history
        "fr" 'helm-gtags-find-rtag
        "fR" 'helm-gtags-resume
        "fs" 'helm-gtags-select
        "fS" 'helm-gtags-show-stack
        "fy" 'helm-gtags-find-symbol
        "fu" 'helm-gtags-update-tags))
(use-package dumb-jump
  :init (setq dumb-jump-selector 'helm)
  :general (my:core::general-def
             "jj" #'dumb-jump-go
             "jp" #'dumb-jump-go-prompt
             "jB" #'dumb-jump-back
             "jG" #'dumb-jump-go-other-window
             "jq" #'dumb-jump-quick-look
             "je" #'dumb-jump-go-prefer-external
             "jE" #'dumb-jump-go-prefer-external-other-window))

;; misc
(use-package srefactor
  :defer t)
(use-package semantic
  :commands (semantic-mode)
  :hook (prog-mode . semantic-mode))
(use-package stickyfunc-enhance
  :defer t
  :init (add-to-list 'semantic-default-submodes
                     #'global-semantic-stickyfunc-mode))
(use-package dap-mode
  :defer t
  :init (progn
          (general-create-definer my:code::dap-def
            :wrapping my:core::mmode-general-def
            "d" '(:ignore t :wk "dap")
            "d." #'my:code::dap-hercules
            "dn" #'dap-next
            "di" #'dap-step-in
            "do" #'dap-step-out
            "dc" #'dap-continue
            "dr" #'dap-restart-frame
            "dss" #'dap-switch-session
            "dst" #'dap-switch-thread
            "dsf" #'dap-switch-stack-frame
            "dsl" #'dap-ui-locals
            "dsb" #'dap-ui-breakpoints
            "dsS" #'dap-ui-sessions
            "dbt" #'dap-breakpoint-toggle
            "dba" #'dap-breakpoint-add
            "dbd" #'dap-breakpoint-delete
            "dbc" #'dap-breakpoint-condition
            "dbh" #'dap-breakpoint-hit-condition
            "dbl" #'dap-breakpoint-log-message
            "dee" #'dap-eval
            "der" #'dap-eval-region
            "des" #'dap-eval-thing-at-point
            "deii" #'dap-ui-inspect
            "deir" #'dap-ui-inspect-region
            "deis" #'dap-ui-inspect-thing-at-point
            "dQ" #'dap-disconnect)
          (hercules-def
           :toggle-funs #'my:code::dap-hercules
           :keymap 'my:code:dap-hercules-map
           :flatten t
           :config '(general-def
                      :prefix-map 'my:code:dap-hercules-map
                      "n" #'dap-next
                      "i" #'dap-step-in
                      "o" #'dap-step-out
                      "c" #'dap-continue
                      "r" #'dap-restart-frame
                      "ss" #'dap-switch-session
                      "st" #'dap-switch-thread
                      "sf" #'dap-switch-stack-frame
                      "sl" #'dap-ui-locals
                      "sb" #'dap-ui-breakpoints
                      "sS" #'dap-ui-sessions
                      "bt" #'dap-breakpoint-toggle
                      "ba" #'dap-breakpoint-add
                      "bd" #'dap-breakpoint-delete
                      "bc" #'dap-breakpoint-condition
                      "bh" #'dap-breakpoint-hit-condition
                      "bl" #'dap-breakpoint-log-message
                      "ee" #'dap-eval
                      "er" #'dap-eval-region
                      "es" #'dap-eval-thing-at-point
                      "eii" #'dap-ui-inspect
                      "eir" #'dap-ui-inspect-region
                      "eis" #'dap-ui-inspect-thing-at-point
                      "q" #'my:code::dap-hercules
                      "Q" #'dap-disconnect)))
  :config (dap-mode))
(use-package flycheck
  :init
  (progn
    (setq flycheck-standard-error-navigation t
          flycheck-display-errors-function nil
          flycheck-checker-error-threshold 1000
          flycheck-indication-mode nil
          flycheck-textlint-config
          (concat user-emacs-directory
                  "my:config/my:textlintrc.json")
          flycheck-textlint-plugin-alist
          '((LaTeX-mode . "latex")
            (org-mode . "@textlint/text")
            (markdown-mode . "@textlint/markdown")
            (gfm-mode . "@textlint/markdown")
            (t . "@textlint/text")))
    (defun my:code::toggle-flycheck-error-list ()
      "Toggle flycheck's error list window.
If the error list is visible, hide it.  Otherwise, show it."
      (interactive)
      (let ((window (flycheck-get-error-list-window)))
        (if window
            (quit-window nil window)
          (flycheck-list-errors)))))
  :config (flycheck-add-next-checker 'tex-chktex 'textlint)
  :gfhook ('prog-mode-hook #'flycheck-mode)
  :general
  (my:core::general-def
    "t" '(:ignore t :wk "toggle")
    "tf" #'flycheck-mode

    "e" '(:ignore t :wk "error")
    "en" #'flycheck-next-error
    "ep" #'flycheck-previous-error
    "eb" #'flycheck-buffer
    "ec" #'flycheck-clear
    "eH" #'flycheck-describe-checker
    "el" #'my:code::toggle-flycheck-error-list
    "es" #'flycheck-select-checker
    "eS" #'flycheck-set-checker-executable
    "ev" #'flycheck-verify-setup
    "ey" #'flycheck-copy-errors-as-kill
    "ex" #'flycheck-explain-error-at-point))
(use-package helm-flycheck
  :disabled t
  :after flycheck
  :general (my:core::general-def
             "e" '(:ignore t :wk "error")
             "eh" #'helm-flycheck))

(provide 'my:code)
