;; -*- lexical-binding: t; -*-
(require 'my:core)
(require 'my:code)

(use-package geiser
  :after evil-lisp-state
  :defer t
  :commands run-geiser
  :mode ("\\.scm\\'" . scheme-mode)
  :mode ("\\.ss\\'"  . scheme-mode)
  :config
  (progn
    (general-create-definer my:scheme::general-def
      :wrapping my:core::mmode-general-def
      :prefix-map my:scheme:map
      :prefix-command my:scheme::command
      :keymaps '(scheme-mode-map)
      :major-modes t)
    ;; prefixes
    (my:scheme::general-def
      "'"  'geiser-mode-switch-to-repl
      ","  'lisp-state-toggle-lisp-state

      "c" '(:ignore t :wk "compile")
      "cc" 'geiser-compile-current-buffer
      "cp" 'geiser-add-to-load-path

      "e" '(:ignore t :wk "eval")
      "eb" 'geiser-eval-buffer
      "ee" 'geiser-eval-last-sexp
      "ef" 'geiser-eval-definition
      "el" 'lisp-state-eval-sexp-end-of-line
      "er" 'geiser-eval-region

      "g" '(:ignore t :wk "geiser")
      "gb" 'geiser-pop-symbol-stack
      "gm" 'geiser-edit-module
      "gn" 'next-error
      "gN" 'previous-error

      "h" '(:ignore t :wk "help")
      "hh" 'geiser-doc-symbol-at-point
      "hd" 'geiser-doc-look-up-manual
      "hm" 'geiser-doc-module
      "h<" 'geiser-xref-callers
      "h>" 'geiser-xref-callees

      "i" '(:ignore t :wk "insert")
      "il" 'geiser-insert-lambda

      "m" '(:ignore t :wk "macro")
      "me" 'geiser-expand-last-sexp
      "mf" 'geiser-expand-definition
      "mx" 'geiser-expand-region

      "s" '(:ignore t :wk "shell")
      "si" 'geiser-mode-switch-to-repl
      "sb" 'geiser-eval-buffer
      "sB" 'geiser-eval-buffer-and-go
      "sf" 'geiser-eval-definition
      "sF" 'geiser-eval-definition-and-go
      "se" 'geiser-eval-last-sexp
      "sr" 'geiser-eval-region
      "sR" 'geiser-eval-region-and-go
      "ss" 'geiser-set-scheme)))

(provide 'my:scheme)
